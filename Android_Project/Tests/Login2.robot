*** Settings ***
Library         AppiumLibrary
Resource        ../Resources/password.robot

*** Variables ***
#*** Test Variables ***
&{USER1-DETAILS}            email=avinashchhetri1997@gmail.com      password=${PASSWORD}
&{USER2-DETAILS}            email=lexeta6221@suggerin.com           password=${PASSWORD2}

#*** Login Page ***
${LOGIN-EMAIL-FIELD}        id=chat21.android.demo:id/email
${LOGIN-PASSWORD-FIELD}     id=chat21.android.demo:id/password
${LOGIN-SIGNIN-BUTTON}      id=chat21.android.demo:id/login

#*** Main Page ***
${MAIN-HOME-TAB}            //android.widget.TextView[@text="HOME"]
${MAIN-PROFILE-TAB}            //android.widget.TextView[@text="PROFILE"]

#*** Profile Page ***
${PROFIlE-LOGOUT-BUTTON}       id=chat21.android.demo:id/logout

*** Test Cases ***
Login2

    wait until page contains element    ${LOGIN-EMAIL-FIELD}

     Input Text        ${LOGIN-EMAIL-FIELD}     ${USER2-DETAILS}[email]
     Input Text        ${LOGIN-PASSWORD-FIELD}     ${USER2-DETAILS}[password]
     click Element     ${LOGIN-SIGNIN-BUTTON}
     wait until page contains element               ${MAIN-HOME-TAB}
