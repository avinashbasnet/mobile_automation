*** Settings ***
Library         AppiumLibrary
Resource        password.robot

*** Variables ***

#*** Test Variables ***
&{USER1-DETAILS}            email=avinashchhetri1997@gmail.com      password=${PASSWORD}
&{USER2-DETAILS}            email=lexeta6221@suggerin.com           password=${PASSWORD2}

${APPIUM-PORT-DEVICE1}              4723
${APPIUM-PORT-DEVICE2}              4725

#*** Android 10 VARIABLES ***
${ANDROID10-CONTINUE-BUTTON}            id=com.android.permissioncontroller:id/continue_button
${ANDROID10-OK-BUTTON}                  //android.widget.Button[@text="OK"]



#*** Application Variables ***
${CHAT21-APPLICATION-ID}            chat21.android.demo
${CHAT21-APPLICATION-ACTIVITY}      chat21.android.demo.SplashActivity

#*** Login Page ***
${LOGIN-EMAIL-FIELD}        id=${CHAT21-APPLICATION-ID}:id/email
${LOGIN-PASSWORD-FIELD}     id=${CHAT21-APPLICATION-ID}:id/password
${LOGIN-SIGNIN-BUTTON}      id=${CHAT21-APPLICATION-ID}:id/login

#*** Main Page ***
${MAIN-HOME-TAB}            //android.widget.TextView[@text="HOME"]
${MAIN-PROFILE-TAB}            //android.widget.TextView[@text="PROFILE"]


#*** Profile Page ***
${PROFIlE-LOGOUT-BUTTON}       id=${CHAT21-APPLICATION-ID}:id/logout

*** Keywords ***

Open Chat21 Application
    [Arguments]     ${APPIUM-PORT}= ${APPIUM-PORT-DEVICE1}
    open application  http://localhost:${APPIUM-PORT}/wd/hub   platformName=Android    deviceName=emulator-5554    appPackage=${CHAT21-APPLICATION-ID}  appActivity=${CHAT21-APPLICATION-ACTIVITY}    automationName=Uiautomator2
    ${ALERT}            Run Keyword And Return Status           page should not contain element        ${ANDROID10-CONTINUE-BUTTON}
    Run Keyword If      '${ALERT}' =='False'         Bypass Android 10 Alerts

Open Chat21 Application On First Device
    Open Chat21 Application                     ${APPIUM-PORT-DEVICE1}

Open Chat21 Application On Second Device
    Open Chat21 Application                     ${APPIUM-PORT-DEVICE2}


Bypass Android 10 Alerts
    wait until page contains element    ${ANDROID10-CONTINUE-BUTTON}
    click element                       ${ANDROID10-CONTINUE-BUTTON}
    wait until page contains element    ${ANDROID10-OK-BUTTON}
    click element                       ${ANDROID10-OK-BUTTON}



Signin With User
     [Arguments]        ${EMAIL}    ${USERPASSWORD}
     Input User Email       ${EMAIL}
     Input User Password    ${USERPASSWORD}
     submit login
     verify login is successful

Input User Email
    [Arguments]     ${EMAIL}
    Verify Login Email Field Is Displayed
     Input Text        ${LOGIN-EMAIL-FIELD}    ${EMAIL}

Input User Password
    [Arguments]     ${USERPASSWORD}
    Input Text        ${LOGIN-PASSWORD-FIELD}     ${USERPASSWORD}

Submit Login
    Click Element     ${LOGIN-SIGNIN-BUTTON}

verify login is successful
    wait until page contains element                ${MAIN-HOME-TAB}



Logout with user
    go to profile page
    click the logout button
    verify login email field is displayed

Go to profile page
    click element                                   ${MAIN-PROFILE-TAB}
    wait until page contains element                ${PROFILE-LOGOUT-BUTTON}

click the logout button
    CLICK ELEMENT                                  ${PROFILE-LOGOUT-BUTTON}

Verify Login Email Field Is Displayed
    wait until page contains element               ${LOGIN-EMAIL-FIELD}